typedef struct
{
  short unsigned int key;
  char payload[4];
} sort_data;

#define klt(v1,v2) ((v1-v2) & 0x8000)
#define kle(v1,v2) (((v2-v1) & 0x8000)==0)

#define NM 11
#define SORT_LEN (1<<NM)
#define MAX_DEL (SORT_LEN)
#define TEST_LEN (MAX_DEL*100)
extern sort_data l_smem[NM][SORT_LEN / 2];
extern sort_data r_smem[NM][SORT_LEN / 2];
sort_data heap_sort (sort_data);
