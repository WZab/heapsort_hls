#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "wz_hsort.h"

/* How we assign memory address to each level?
   N - level
   NM - max level
   SORT_LEN = 2^(NM+1)
   base address 
   How we calculate the address of the node?
   2^N - base address which is also the "LEFT" address
   2^N + 2^(N-1) - "RIGHT" address
   How we create the "offset"? - we simply collect the bits from
   the previous "left<->right" decisions.
   
*/



int main()
{
   int i,j;
   for(i=0;i<NM;i++)
     for(j=0;j<SORT_LEN/2;j++) {
       l_smem[i][j].key=0;
       r_smem[i][j].key=0;
       }
   FILE * fout=fopen("dta.out","w");
   sort_data r;
   //memset(sort_mem,0,sizeof(sort_mem));
   for(i=0;i<TEST_LEN;i++) {
	  sort_data vin;
	  strcpy(vin.payload,"ak");
	  vin.key=i+1+rand()/(RAND_MAX/MAX_DEL);
      r=heap_sort(vin);
      //for(j=0;j<SORT_LEN;j++) printf("%d:%d,",j,sort_mem[j]);
      if(0){
        int i,j;
        for(i=0;i<NM;i++) {
           printf("%2.2dL:",i);
           for(j=0;j<SORT_LEN/2;j++) {
             printf("%2.2d,",l_smem[i][j].key);
           }
           printf("\n%2.2dR:",i);
           for(j=0;j<SORT_LEN/2;j++) {
             printf("%2.2d,",r_smem[i][j].key);
           }
           printf("\n");
        }
      }
      printf("in=%d out=%d\n\n",vin.key,r.key);
      fprintf(fout,"%d\n",r.key);
   }   
   fclose(fout);
}

