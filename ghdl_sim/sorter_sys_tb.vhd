-------------------------------------------------------------------------------
-- Title      : Testbench for design "HLS heap-sorter"
-- Project    : heap-sorter
-------------------------------------------------------------------------------
-- File       : sorter_sys_tb.vhd
-- Author     : Wojciech M. Zabolotny <wzab@ise.pw.edu.pl>
-- Company    : 
-- Created    : 2010-05-14
-- Last update: 2018-04-30
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2010 Wojciech M. Zabolotny
-- This file is published under the BSD license, so you can freely adapt
-- it for your own purposes.
-- Additionally this design has been described in my article:
--    Wojciech M. Zabolotny, "Dual port memory based Heapsort implementation
--    for FPGA", Proc. SPIE 8008, 80080E (2011); doi:10.1117/12.905281
-- I'd be glad if you cite this article when you publish something based
-- on my design.
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2010-05-14  1.0      wzab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;
library work;
use work.sys_config.all;
use work.sorter_pkg.all;


-------------------------------------------------------------------------------

entity sorter_sys_tb is

end entity sorter_sys_tb;

-------------------------------------------------------------------------------

architecture sort_tb_beh of sorter_sys_tb is

  -- component ports
  signal din   : T_DATA_REC := DATA_REC_INIT_DATA;
  signal dout  : T_DATA_REC := DATA_REC_INIT_DATA;
  signal we    : std_logic  := '0';
  signal dav   : std_logic  := '0';
  signal rst_n : std_logic  := '0';
  signal rst_p : std_logic  := '0';
  signal ready : std_logic  := '0';

  component heap_sort is
    port (
      ap_clk            : IN  STD_LOGIC;
      ap_rst            : IN  STD_LOGIC;
      ap_start          : IN  STD_LOGIC;
      ap_done           : OUT STD_LOGIC;
      ap_idle           : OUT STD_LOGIC;
      ap_ready          : OUT STD_LOGIC;
      agg_result        : OUT STD_LOGIC_VECTOR (47 downto 0);
      agg_result_ap_vld : OUT STD_LOGIC;
      val_r             : IN  STD_LOGIC_VECTOR (47 downto 0);
      val_r_ap_vld      : IN  STD_LOGIC;
      val_r_ap_ack      : OUT STD_LOGIC);
  end component heap_sort;

  signal ap_clk            : STD_LOGIC := '0';
  signal ap_rst            : STD_LOGIC := '0';
  signal ap_start          : STD_LOGIC := '0';
  signal ap_done           : STD_LOGIC := '0';
  signal ap_idle           : STD_LOGIC := '0';
  signal ap_ready          : STD_LOGIC := '0';
  signal agg_result        : STD_LOGIC_VECTOR (47 downto 0) := (others => '0');
  signal agg_result_ap_vld : STD_LOGIC := '0';
  signal val_r             : STD_LOGIC_VECTOR (47 downto 0) := (others => '0');
  signal val_r_ap_vld      : STD_LOGIC := '0';
  signal val_r_ap_ack      : STD_LOGIC := '0';
  
  -- clock
  signal Clk : std_logic := '1';

  signal end_sim : boolean              := false;
  signal div     : integer range 0 to 8 := 0;
  
begin  -- architecture sort_tb_beh

  ap_clk <= Clk;
  ap_rst <= not rst_n;
  
  heap_sort_1: entity work.heap_sort
    port map (
      ap_clk            => ap_clk,
      ap_rst            => ap_rst,
      ap_start          => ap_start,
      ap_done           => ap_done,
      ap_idle           => ap_idle,
      ap_ready          => ap_ready,
      agg_result        => agg_result,
      agg_result_ap_vld => agg_result_ap_vld,
      val_r             => val_r,
      val_r_ap_vld      => val_r_ap_vld,
      val_r_ap_ack      => val_r_ap_ack);

  -- clock generation
  Clk <= not Clk after 10 ns when end_sim = false else '0';

  -- waveform generation
  WaveGen_Proc : process
    file events_in       : text open read_mode is "events.in";
    variable input_line  : line;
    file events_out      : text open write_mode is "events.out";
    variable output_line : line;
    variable rec         : T_DATA_REC;
    variable skey       : std_logic_vector(DATA_REC_SORT_KEY_WIDTH-1 downto 0);
    variable spayload : std_logic_vector(DATA_REC_PAYLOAD_WIDTH-1 downto 0);
  begin
    -- insert signal assignments here

    wait until Clk = '1';
    wait for 31 ns;
    rst_n <= '1';
    --wait until ready = '1';
    loop
      wait until Clk = '0';
      wait until Clk = '1';
      if (ap_idle = '1') or (ap_ready = '1') then
        exit when endfile(events_in);
        readline(events_in, input_line);
        read(input_line, skey);
        read(input_line, spayload);
        rec.d_key := unsigned(skey);
        rec.d_payload := spayload;
        val_r <= tdrec2stlv(rec);
        din <= rec;
        val_r_ap_vld <= '1';
        ap_start <= '1';
      end if;
      if agg_result_ap_vld = '1' then
        -- Process read event
        rec := stlv2tdrec(agg_result);
        dout <= rec;
        write(output_line, std_logic_vector(rec.d_key));
        write(output_line,string'(" "));
        write(output_line, std_logic_vector(rec.d_payload));
        writeline(events_out, output_line);
      end if;
    end loop;
    wait;
  end process WaveGen_Proc;
  

end architecture sort_tb_beh;

-------------------------------------------------------------------------------

configuration sorter_sys_tb_sort_tb_beh_cfg of sorter_sys_tb is
  for sort_tb_beh
  end for;
end sorter_sys_tb_sort_tb_beh_cfg;

-------------------------------------------------------------------------------
